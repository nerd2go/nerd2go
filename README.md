Nerd2Go is Brisbane’s #1 Top Rated & Trusted Computer Repair Specialists. We offer onsite, remote and in-shop computer repairs across Brisbane. We can fix your IT problems in the comfort of your home or office ($0 callout fees), or you can bring your device to us.

Address: 4/55 McDougall St, Milton, QLD 4064, Australia

Phone: +61 7 3040 2067

Website: https://nerd2go.com.au
